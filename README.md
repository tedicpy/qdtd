# Web Quien defiende tus datos - Paraguay

Desarrollo de la web de Quien defiende tus datos utilizando https://www.11ty.dev

Además está dockerizado:

## Correr el servidor de desarrollo

```
docker compose up -d
```

Ya hace el "build" pero si por algún motivo algo no queda bien generado, se puede hacer al vuelo:

## Hacer build

Cuando el servidor de desarrollo está corriendo, se ejecuta el siguiente comando:

```
docker compose exec app npm run build
```

Esto genera los archivos estáticos en la carpeta public y eso se sube al servidor. 
