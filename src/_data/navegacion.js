module.exports = {
  es: {
    investigacion: "Investigación",
    investigacionURL: "#investigacion",
    empresas: "Empresas",
    empresasURL: "#empresas",
    organizaciones: "Organizaciones",
    organizacionesURL: "#organizaciones",
    ediciones: {
      edicion2017: "Edición 2017",
      edicion2017URL: "/edicion2017/",
      edicion2020: "Edición 2020",
      edicion2020URL: "/edicion2020/",
      edicion2022: "Edición 2022",
      edicion2022URL: "/edicion2022/",
      edicion2024: "Edición 2024",
      edicion2024URL: "/",
    },
    contacto: "Contacto",
    contactoURL: "#contacto",
  },
};
