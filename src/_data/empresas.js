module.exports = {
  es: {
    titulo: "Empresas relevadas",
    tigo: "<strong>Tigo</strong> Telefonía",
    personal: "<strong>Personal</strong> Telefonía",
    copaco: "<strong>Copaco</strong> Internet Hogar / Telefonía",
    claro: "<strong>Claro</strong> Telefonía",
    vox: "<strong>Vox</strong> Telefonía",
    chaconet: "<strong>Chaconet</strong> Internet Hogar / Telefonía",
  },
};
