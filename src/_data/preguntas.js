module.exports = {
  es: {
    pregunta1IMG: "/imagenes/flyers/1_como_protegen.png",
    pregunta1: "¿Cómo protegen Tigo, Personal, Copaco y Claro tu privacidad?",
    pregunta2IMG: "/imagenes/flyers/2_sabes_entregan_datos.png",
    pregunta2: "¿Sabés si estas empresas entregan tus datos sin orden judicial?",
    pregunta3IMG: "/imagenes/flyers/3_te_informan.png",
    pregunta3: "¿Te informan estas empresas si el gobierno pide tus datos?",
    pregunta4IMG: "/imagenes/flyers/4_accesibilidad.png",
    pregunta4: "¿Sabés si estas empresas tienen prácticas y políticas de accesibilidad?",
  },
};