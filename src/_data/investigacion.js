module.exports = {
  es: {
    titulo: "La investigación",
    edicion2017: {
      texto: `<p>"¿Quién Defiende Tus Datos?" es un proyecto de TEDIC (Tecnología y Comunidad), la organización de derechos digitales más importante en Paraguay y EFF (Electronic Frontier Foundation), que busca impulsar buenas prácticas entre las proveedoras de Internet para que protejan los derechos humanos y ofrezcan información clara sobre el uso de los datos de las personas que contratan sus servicios de Internet.</p>

      <p>Se evalúan las 6 empresas de telecomunicaciones más importantes del país, que comprenden la gran mayoría del mercado de telefonía fija, móvil y banda ancha. Estas son: Tigo, Personal, Copaco, Vox, Claro y Chaco Comunicaciones.</p>

      <p>Para la evaluación de las proveedoras de Internet se ha tenido en cuenta los estándares de derechos humanos y empresas, tales como los Principios Rectores sobre las Empresas y Derechos Humanos de la ONU, la [Guía de Implementación de los Principios Rectores para el Sector TIC] elaborada por la Comisión Europea; la Guía de Implementación de la iniciativa Global Network Initiative.</p>

      <p>También se incorporan los resultados de «Who has your back?» el proyecto regional liderado por Electronic Frontier Foundation, con participación de organizaciones como R3D de México, Fundación Karisma de Colombia, Hiperderecho de Perú e InternetLab de Brasil.</p>

      <p>Por otro lado, para este informe, se adoptan los estándares de protección de privacidad expuestos en la Constitución de la República del Paraguay, tratados internacionales ratificados por Paraguay y decisiones de la Corte Suprema y la Corte Interamericana de Derechos Humanos.</p>

      <p>El reto más importante de las proveedoras de Internet en Paraguay es ampliar el compromiso con la transparencia y respetar los derechos humanos. Este documento sirve como una herramienta que refleja la importancia de rendir cuentas sobre los datos personales de quienes contratan estos servicios.</p>

      <p>Este informe continuará periódicamente con evaluaciones realizadas de forma anual. Cada versión revelará la metodología y resultados, garantizando que los mismos estén dentro del alcance y las posibilidades de cumplimiento por parte de las empresas evaluadas para que defiendan tus datos.</p>`,
      cuadro: "/imagenes/cuadro2017.jpg",
      linkPDF: "/imagenes/pdfs/QDTD-2017.pdf",
    },
    edicion2020: {
      texto: `<p>“Quién defiende tus datos” tiene como objetivo promover la transparencia y las mejores prácticas en los campos de privacidad y protección de datos por parte de las empresas que brindan acceso a Internet en Paraguay. Realizada casi anualmente, cada nueva evaluación está precedida por una revisión de metodología, de modo que los resultados pueden reflejar con mayor precisión el marco legal existente, considerar los problemas emergentes y abarcar las buenas prácticas en las áreas de privacidad y protección de datos personales.</p><p>“Quién defiende tus datos” está inspirado en “Who has your back” de EFF, aunque la metodología difiere de la aplicada en EEUU, ya que la realidad legal de Paraguay es bien diferente a la estadounidense.</p><p>En su segunda edición, el proyecto evaluó a las mismas empresas de telefonía y servicios de Internet que se habían evaluado en 2017: COPACO, VOX, PERSONAL, TIGO y CLARO. Se ha excluido a ChacoNet ya que se tomó el criterio de analizar solamente las empresas que tienen un mercado superior a 15.000 clientes a nivel nacional.</p><p>Se ha habilitado una tabla de resultados para poder comparar el rendimiento de las empresas en las categorías y criterios de evaluación, basados en: su compromiso público con el cumplimiento de la ley; sus adopciones de buenas prácticas favorables a las personas usuarias de servicio de Internet; su transparencia sobre las prácticas y políticas; la accesibilidad en sus plataformas webs, entre otros.</p><p>Cada empresa fue evaluada con base en las 7 categorías señaladas y justificadas a continuación, cuya elaboración tuvo en cuenta los requisitos de la ley vigente en Paraguay y las buenas prácticas internacionales en materia de protección de la privacidad y tratamiento de datos personales. Para esta evaluación, se analizaron los acuerdos de servicio, informes de sostenibilidad y otros documentos que estuvieron disponibles en los sitios web de las empresas hasta el 30 de enero de 2020. También se evaluaron noticias de prensa y medios especializados en la materia.</p><p>Con los resultados preliminares en mano, TEDIC contactó con las empresas y solicitó revisión y comentarios sobre los mismos, con especial énfasis en las metodologías y resultados obtenidos hasta enero 2020. Finalmente estos comentarios y opiniones de las empresas fueron ajustados en la conclusión y en los puntajes correspondientes.</p>`,
      cuadro: "/imagenes/cuadro2020.png",
      linkPDF: "/imagenes/pdfs/QDTD-2020.pdf",
    },
    edicion2022: {
      texto: `<p>El proyecto “Quién defiende tus datos” está inspirado en “Who has your back”1 de EFF, aunque la metodología difiere de la aplicada en EEUU, ya que la realidad legal de Paraguay es bien diferente a la estadounidense.</p>
      <p>
        En su tercera edición, el proyecto evaluó a las mismas empresas de telefonía y servicios de
        Internet que se habían evaluado en 2020: COPACO, VOX, PERSONAL, TIGO y CLARO.
        La investigación se centra en responder las preguntas sobre ¿Cómo ISP protegen nuestra privacidad? ¿Cómo protegen nuestros datos personales? ¿Existen procedimientos claros? ¿Participan
        de forma activa en la defensa de nuestros derechos en las mesas de debate público?
      </p>
      <p>
        La pandemia de COVID-19 nos obligó a depender aún más de los medios tecnológicos. Es por
        ello la importancia de evaluar la tecnología y las telecomunicaciones con perspectiva de derechos de manera recurrente porque son fundamentales en nuestra actual forma de interacción y
        comunicación.
      </p>
      <p>
        Este año, se realizó un pequeño ajuste para que los criterios puedan compararse a nivel regional.
        Se ha habilitado una tabla de resultados para poder comparar el rendimiento de las empresas en
        las categorías y criterios de evaluación, basados en: su compromiso público con el cumplimiento de la ley; sus adopciones de buenas prácticas favorables a las personas usuarias de servicio
        de Internet; su transparencia sobre las prácticas y políticas; la accesibilidad en sus plataformas
        webs, entre otros.
      </p>
      <p>
        Cada empresa fue evaluada con base en las 7 categorías señaladas y justificadas a continuación,
        cuya elaboración tuvo en cuenta los requisitos de la ley vigente en Paraguay y las buenas prácticas internacionales en materia de protección de la privacidad y tratamiento de datos personales.</p>
        <p>
          Para esta evaluación, se analizaron los acuerdos de servicio, informes de sostenibilidad y otros
          documentos que estuvieron disponibles en los sitios web de las empresas hasta enero de 2022.
          También se evaluaron noticias de prensa y medios especializados en la materia.
        </p>
      <p>
        Con los resultados preliminares en mano, TEDIC contactó con las empresas y solicitó revisión y
        comentarios sobre los mismos, con especial énfasis en las metodologías y resultados obtenidos
        hasta enero de 2022. Finalmente estos comentarios y opiniones de las empresas fueron ajustados en la conclusión y en los puntajes correspondientes.
      </p>`,
      cuadro: "/imagenes/cuadro2022.png",
      linkPDF: "/imagenes/pdfs/QDTD-2022_v2.pdf",
      linkHTML: "/investigacion2022/",
      textoHTML: "Ver investigación",
    },
    edicionActual: {
      texto: `<p>El proyecto “Quién defiende tus datos” se basa en la iniciativa “Who has your back” de la Electronic Frontier Foundation (EFF), adaptando su metodología a las normativas y el contexto legal de Paraguay.
      </p>
      <p>
      En su cuarta edición, se analizaron empresas de telefonía e Internet con más de 15,000 clientes en el país: COPACO, VOX, PERSONAL, TIGO y CLARO. La investigación busca responder preguntas como: ¿Cómo gestionan estas empresas la privacidad de sus usuarios? ¿Qué procedimientos utilizan para proteger los datos personales? ¿Participan en debates públicos sobre derechos digitales?
      </p>
      <p>
      El estudio considera informes de Naciones Unidas que destacan la relevancia del acceso a Internet para el ejercicio de derechos humanos, la participación social y la conectividad global. En este contexto, se revisan las prácticas de las empresas de telecomunicaciones para identificar cómo cumplen con estas expectativas.
      </p>
      <p>
      Desde la edición de 2020, se incorporó una tabla de resultados que permite comparar el desempeño de las empresas en las ediciones de 2017, 2020, 2022 y 2024. Los criterios analizados incluyen el cumplimiento de la ley, la implementación de prácticas enfocadas en los usuarios, la transparencia en políticas y procedimientos, y la accesibilidad de las plataformas web.
      </p>`,
      cuadro: "/imagenes/cuadro2024.png",
      linkPDF: "https://www.tedic.org/wp-content/uploads/2025/01/QDTD_Paraguay_2024-WEB.pdf",
      linkHTML: "/investigacion2024/",
      textoHTML: "Ver investigación",
    },
    textoBoton: "PDF",
  },
};
