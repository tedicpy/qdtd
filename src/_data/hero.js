module.exports = {
  es: {
    titulo: "¿QUIÉN DEFIENDE TUS DATOS?",
    descripcion:
      "¿Sabes qué políticas tiene tu compañía proveedora de Internet para cuidar tus datos personales? ¿Conoces qué tipo de prácticas aplican para resguardar tu privacidad? Estas son algunas de las preguntas que nos hacemos en esta investigación sobre las principales empresas proveedoras de Internet en Paraguay",
    cta: "Saber más",
    url: "#",
  },
  en: {
    titulo: "¿QUIÉN DEFIENDE TUS DATOS?",
    descripcion:
      "¿Sabes qué políticas tiene tu compañía proveedora de Internet para cuidar tus datos personales? ¿Conoces qué tipo de prácticas aplican para resguardar tu privacidad? Estas son algunas de las preguntas que nos hacemos en esta investigación sobre las principales empresas proveedoras de Internet en Paraguay",
    cta: "Find out more",
    url: "#",
  },
};
