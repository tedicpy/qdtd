module.exports = {
  es: {
    titulo: "Organizaciones involucradas",
    logoTedic: "/imagenes/logos/logo-tedic.png",
    descripcionTedic:
      "Es una organización sin fines de lucro que desarrolla tecnología cívica y defiende los derechos humanos en Internet en pos de una cultura libre.",
    logoEFF: "/imagenes/logos/logo-eff.png",
    descripcionEFF:
      "Electronic Frontier Foundation es una organización internacional sin fines de lucro que desde 1990 defiende la libertad de expresión y la privacidad en el entorno digital.",
  },
};
