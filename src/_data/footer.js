module.exports = {
  es: {
    titulo: "Contactos",
    subtitulo: "No dudes en aportar sugerencias o comentarios",
    textoTwitter: "@tedicpy",
    urlTW: "https://twitter.com/tedicpy",
    iconoTW: "../imagenes/iconos/twitter-svgrepo-com.svg",
    textoForm: "Formulario",
    urlForm: "https://www.tedic.org/contacto",
    iconoForm: "../imagenes/iconos/email-svgrepo-com.svg",
  },
};
