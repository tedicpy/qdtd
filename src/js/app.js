
// MOSTRAR / OCULTAR BARRA DE NAVEGACION AL HACER SCROLL UP / DOWN
const body = document.body;
const encabezado = document.querySelector("encabezado");

let lastScroll = 0;

window.addEventListener("scroll", () => {
  const currentScroll = window.scrollY;

  if (currentScroll <= 0) {
    body.classList.remove("scroll-up");
  }

  if (currentScroll > lastScroll && !body.classList.contains("scroll-down")) {
    body.classList.remove("scroll-up");
    body.classList.add("scroll-down");
  }

  if (currentScroll < lastScroll && body.classList.contains("scroll-down")) {
    body.classList.remove("scroll-down");
    body.classList.add("scroll-up");
  }

  lastScroll = currentScroll;
});

// MENU

const menuToggle = document.querySelector(".toggle-menu");
const containerMenu = document.querySelector(".menu-navegacion");
const itemsMenuDesktop = document.querySelector(".con-submenu");

// Abrir / cerrar menu al hacer click en el toggle
menuToggle.addEventListener("click", function () {
  containerMenu.classList.toggle("menu-cerrado");
  menuToggle.classList.toggle("menu-cerrado");
});

// Ponerle la clase menu-abierto a cada menu item, y sacarla de los siblings cuando sea necesario
itemsMenuDesktop.addEventListener("click", () => {
  itemsMenuDesktop.classList.toggle("menu-abierto");

});

function removeActiveClasses() {
  itemsMenuDesktop.classList.remove("menu-abierto");
}

// Ocultar el menu al hacer click fuera del area del mismo
document.addEventListener("click", (e) => {
  if (!itemsMenuDesktop.contains(e.target) && !menuToggle.contains(e.target)) {
    itemsMenuDesktop.classList.remove("menu-abierto");
  }
});
