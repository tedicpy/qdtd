FROM node:22

ENV CODE /usr/src/app
ENV PATH="$PATH:$CODE/node_modules/.bin"

WORKDIR $CODE 

COPY package.json .
RUN npm install

# Copy code to conatiner volume
COPY . .

CMD ["npx", "@11ty/eleventy", "--serve"]
