module.exports = function (eleventyConfig) {
  eleventyConfig.addWatchTarget("./src/sass");
  eleventyConfig.addPassthroughCopy("./src/css");
  eleventyConfig.addPassthroughCopy("./src/imagenes");
  eleventyConfig.addPassthroughCopy("./src/tipografia");
	eleventyConfig.addPassthroughCopy({ "./src/rootfiles": "/" });
  eleventyConfig.addPassthroughCopy("./src/js");
  return {
    markdownTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dir: {
      input: "src",
      output: "public",
    },
  };
};
